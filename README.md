# README #

### Steps for DEV Mode Deployment ###
Install NodeJs
Clone the repo with specific branch
run "yarn && yarn start" 
yarn is required only for first time. 

### Steps for PROD Mode Deployment ###
Install NodeJs
Clone the repo with specific branch
Run following commands - 
	yarn && yarn build
	sudo npm build -g forever
	forever start server.js
Now hit {INSTANCE_IP}:9000 in the browser. 



### IMPORTANT -> Delete AppSync.js file once Authorization is implemented